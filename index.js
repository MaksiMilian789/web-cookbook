﻿var express = require('express');
var app = express();
const mysql = require('mysql');
var cookieParser = require('cookie-parser');
const jsonParser = express.json();

const PORT = process.env.PORT || 3000

const conn = mysql.createConnection({
    host: "server7.hosting.reg.ru",
    user: "u1353884_default",
    database: "u1353884_default",
    password: "6_Xk193K"
});

app.listen(PORT, () => {
    console.log("Server has been started");
})

app.use(cookieParser('PMI-81'))

app.get('/', function (req, res) {
    //res.cookie('auth', 123, { maxAge: 28800000, signed: true });
    res.sendFile(__dirname + '/index.html');
});

app.use(express.static(__dirname + "/public"));

connections = [];

conn.connect(function (err) {
    if (err) {
        return console.error("Ошибка: " + err.message);
    }
    else {
        console.log("Подключение к серверу MySQL успешно установлено");
    }
});

//чек куки на главной странице
app.get('/check', function (req, res) {

    var check = req.signedCookies.auth;

    if (check == undefined) {
        console.log("connect undefined");
        res.send('!');
    }
    else {
        console.log("connect " + check);

        //кинуть пользователя
        conn.query('SELECT Users.id, Users.login FROM Users ' +
            'WHERE Users.login = ?', check, (error, result) => {
                if (error) throw error;
                var r = JSON.parse(JSON.stringify(result));
                res.send(r);
            });
    }
});

//чек куки на странице холодильника
app.get('/fridge_page.html/check', function (req, res) {

    var check = req.signedCookies.auth;

    if (check == undefined) {
        console.log("connect undefined");
        res.send('!');
    }
    else {
        console.log("connect " + check);

        //кинуть пользователя
        conn.query('SELECT Users.id, Users.login FROM Users ' +
            'WHERE Users.login = ?', check, (error, result) => {
                if (error) throw error;
                var r = JSON.parse(JSON.stringify(result));
                res.send(r);
            });
    }
});

var ID_res;

//принять id рецепта на выдачу
app.get('/id_res', function (req, res) {
    ID_res = req.query.id;
});

//чек куки на странице рецепта
app.get('/recipe_page.html/check', function (req, res) {

    var check = req.signedCookies.auth;

    if (check == undefined) {
        console.log("connect undefined");
        res.send('!');
    }
    else {
        console.log("connect " + check);
        res.send(check);
    }
});

//запрос на удаление куки
app.get("/exit", (req, res) => {
    var check = req.signedCookies.auth;
    console.log("exit " + check);
    res.clearCookie('auth', { signed: true }).send("sucsessful exit");
});

//запрос на массив категорий блюд
app.get("/Category_dish", (req, res) => {
    conn.query('SELECT * FROM Category_dish', (error, result) => {
        if (error) throw error;
        res.send(JSON.parse(JSON.stringify(result)));
    });
});

//запрос на массив типов (г, мл, шт)
app.get("/Units", (req, res) => {
    conn.query('SELECT * FROM Units', (error, result) => {
        if (error) throw error;
        res.send(JSON.parse(JSON.stringify(result)));
    });
});

//запрос на массив категорий продуктов
app.get("/Category_products", (req, res) => {
    conn.query('SELECT * FROM Category_products', (error, result) => {
        if (error) throw error;
        res.send(JSON.parse(JSON.stringify(result)));
    });
});

//запрос на массив продуктов
app.get("/Products", (req, res) => {
    conn.query('SELECT Products.id as c_id, Products.name as c_name, Category_products.name as c_category, Units.name as c_unit ' +
        'FROM Products, Category_products, Units ' +
        'WHERE Products.category = Category_products.ID and Products.unit = Units.ID', (error, result) => {
            if (error) throw error;
            res.send(JSON.parse(JSON.stringify(result)));
        });
});

//авторизация
app.get("/Auth", (req, res) => {
    var login = req.query.login;
    var password = req.query.password;
    var data = [login, password];
    //const data = ["Admin", "Admin"];
    console.log(data);
    conn.query('SELECT Users.id FROM Users ' +
        'WHERE Users.login = ? and Users.password = ?', data, (error, result) => {
            if (error) throw error;
            var r = JSON.parse(JSON.stringify(result));
            if (r.length != 0) {
                res.cookie('auth', login, { maxAge: 28800000, signed: true });
                res.send(r);
            }
            else {
                res.send('!');
            }
        });
});

//запрос на краткие рецепты
app.get("/Mini_recipes", (req, res) => {
    conn.query('SELECT Recipes.id, Recipes.name, Recipes.img, Recipes.descr, Recipes.category, Recipes.ingredients_id, Recipes.ingredients_count FROM Recipes ', (error, result) => {
        if (error) throw error;
        res.send(JSON.parse(JSON.stringify(result)));
    });
});

//запрос на добавление рецепта
app.post("/save_recipe", jsonParser, (req, res) => {
    var name = req.body.name;
    var img = req.body.img;
    var descr = req.body.descr;
    var recipe = req.body.recipe;
    var category = req.body.category;
    var ingredients_id = req.body.ingredients_id;
    var ingredients_count = req.body.ingredients_count;
    var data = [name, img, descr, recipe, category, ingredients_id, ingredients_count];
    console.log(data);
    conn.query('INSERT INTO Recipes(name, img, descr, recipe, category, ingredients_id, ingredients_count) VALUES(?, ?, ?, ?, ?, ?, ?)', data, (error, result) => {
        if (error) throw error;
    });
});

//запрос на добавление продукта
app.post("/save_product", jsonParser, (req, res) => {
    var name = req.body.name;
    var category = req.body.category;
    var unit = req.body.unit;
    var data = [name, category, unit];
    console.log(data);
    conn.query('INSERT INTO Products(name, category, unit) VALUES(?, ?, ?)', data, (error, result) => {
        if (error) throw error;
    });
});

//регистрация
app.post("/Reg", jsonParser, (req, res) => {
    var login = req.body.login;
    var password = req.body.password;

    console.log(111);
    console.log(login + password);

    var ingredients_id = "{ \"0\": [] }";
    var ingredients_count = "{ \"0\": [] }";
    var data = [login, password, ingredients_id, ingredients_count];
    conn.query('SELECT Users.id FROM Users ' +
        'WHERE Users.login = ?', login, (error, result) => {
            if (error) throw error;

            var r = JSON.parse(JSON.stringify(result));
            console.log(r);

            if (r.length != 0) {
                //такой уже существует
                res.send('!');
            }
            else {
                //регистрация успешна
                conn.query('INSERT INTO Users(login, password, ingredients_id, ingredients_count) VALUES(?, ?, ?, ?)', data, (error, result) => {
                    if (error) throw error;
                });
                res.cookie('auth', login, { maxAge: 28800000, signed: true });
                res.send('+');
            }
        });
});

//получаю список продуктов с id
temp_ingr = [];
conn.query('SELECT Products.id as c_id, Products.name as c_name, Category_products.name as c_category, Units.name as c_unit ' +
    'FROM Products, Category_products, Units ' +
    'WHERE Products.category = Category_products.ID and Products.unit = Units.ID', (error, result) => {
        if (error) throw error;
        temp_ingr = JSON.parse(JSON.stringify(result));
    });

//запрос на формирование холодильника
app.get("/Fridge", (req, res) => {
    var data = req.query.id;
    //var data = 1;
    //получаю пользователя по id
    conn.query('SELECT Users.ingredients_id, Users.ingredients_count FROM Users ' +
        'WHERE Users.id = ?', data, (error, result) => {
            if (error) throw error;

            var user = JSON.parse(JSON.stringify(result));
            user_products = JSON.parse(user[0].ingredients_id)[0];
            user_counts = JSON.parse(user[0].ingredients_count)[0];
            //console.log(user_products);

            let hol = [];

            user_products.forEach(function (item, i, user_products) {

                let temp = {
                    id: item,
                    name: temp_ingr.find(x => x.c_id === item).c_name,
                    unit: temp_ingr.find(x => x.c_id === item).c_unit,
                    count: user_counts[i]
                };

                hol.push(temp)
            });

            console.log(hol);
            res.send(hol);

        });
});

//запрос на изменение холодильника
app.post("/Fridge_change", jsonParser, (req, res) => {
    var id = req.body.id;
    var ingredients_id = req.body.ingredients_id;
    var ingredients_count = req.body.ingredients_count;
    var data = [ingredients_id, ingredients_count, id];
    conn.query('UPDATE Users SET Users.ingredients_id = ?, Users.ingredients_count = ? ' +
        'WHERE Users.id = ?', data, (error, result) => {
            if (error) throw error;
        });
});

//запрос на полный рецепт
app.get("/Full_recipes", (req, res) => {
    //var data = 16;
    var data = ID_res;
    conn.query('SELECT Recipes.id, Recipes.name, Recipes.img, Recipes.descr, Recipes.recipe, Recipes.category, Recipes.ingredients_id, Recipes.ingredients_count FROM Recipes WHERE Recipes.id = ?', data, (error, result) => {
        if (error) throw error;

        //получить названия и ед измерения по id
        var prod = JSON.parse(JSON.stringify(result));
        user_products = JSON.parse(prod[0].ingredients_id)[0];

        //console.log(prod);

        let p1 = [];
        let p2 = [];

        user_products.forEach(function (item, i, user_products) {

            var temp1 = temp_ingr.find(x => x.c_id === item).c_name;
            var temp2 = temp_ingr.find(x => x.c_id === item).c_unit;

            p1.push(temp1);
            p2.push(temp2);
        });

        var p11 = JSON.stringify(p1);
        p11 = "{\"0\":" + p11 + "}";

        var p22 = JSON.stringify(p2);
        p22 = "{\"0\":" + p22 + "}";

        let temp = {
            name: prod[0].name,
            img: prod[0].img,
            descr: prod[0].descr,
            recipe: prod[0].recipe,
            category: prod[0].category,
            ingredients_name: p11,
            ingredients_count: prod[0].ingredients_count,
            ingredients_unit: p22
        };

        //console.log(temp);

        res.send(JSON.parse(JSON.stringify(temp)));
    });
});